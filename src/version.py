"""
Provides arm's version and release date.
"""

VERSION = '1.4.3_dev'
LAST_MODIFIED = "April 4, 2011"

